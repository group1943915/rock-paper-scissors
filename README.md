# Rock Paper Scissors

## Game

This is a simple console Rock Paper Scissors game. Nothing to say more, it is intuitive.

## First

As it is my first project on Gitlab, I am just testing its features. :) [This is a reference](#example  )

## Example

From what I see, markdown is easy to handle. 

### List

This is a list :
- *Item in italics*
- **Other item bold**
- ~~Another one strikethrough~~
- [ ] This is a box !
- [X] This is a crossed box !

### Ordered list

1. First item
2. Second item
3. Third item

rule :
---

### Mathematics

- $E=mc^2$
- $10^2$
- H<sub>2</sub>O

This is a footnote : here.[^1]  

> This is a blockquote !

```python
print("Hello World !")
```

|This is a table or what ?| That's a header| 
|---|---|
|A line| of content |
--- 

This line ends by a break.  
I don't know how it works.

[^1]: Content of the footnote !
