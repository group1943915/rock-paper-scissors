# type: ignore
import os, time, random
os.system('cls')
print("\n  _______\n---'   ____)\n      (_____)\n      (_____)\n      (____)\n---.__(___)\n")
print("This is Rock Paper Scissors game")
os.system("pause")
playing = True
score = [0, 0, 0]
while playing:
    os.system("cls")
    options = ["Rock","Paper","Scissors"]
    rand = options[random.randint(0,2)]
    player_input = input("You can choose to play Rock (R), Paper (P) or Scissors (S). You can also quit (Q) the game.\n")
    os.system("cls")
    player_input = player_input.lower()
    if player_input in ["quit", "exit", "q", "e"] :
        break
    if player_input in ["rock", "r"] :
        player_input = "Rock"
    elif player_input in ["paper", "p"]:
        player_input = "Paper"
    elif player_input in ["scissors", "s"]:
        player_input = "Scissors"
    else :
        continue
    os.system("cls")
    print(f"Your opponent played {rand} and you played {player_input}.")
    if rand == player_input :
        score[2] += 1
        print("Draw ! Play again !")
    elif rand == "Scissors" and player_input == "Paper" or rand == "Paper" and player_input == "Rock" or rand == "Rock" and player_input == "Scissors":
        score[1] += 1
        print("You lost, try again !")
    else :
        score[0] += 1
        print("You won, well played !\nPlay again !")
    
    print(f"Score : {score[0]} win(s), {score[1]} loss(es), {score[2]} draw(s).")
    os.system("pause")
    continue

for i in range(3):
    os.system("cls")
    print("You chose to quit, goodbye !")
    print(3-i)
    time.sleep(1)
os.system("exit")

    